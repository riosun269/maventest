package example;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;		
import org.openqa.selenium.firefox.FirefoxDriver;		
import org.testng.Assert;		
import org.testng.annotations.Test;

import com.sun.org.apache.xerces.internal.impl.io.MalformedByteSequenceException;

import org.testng.annotations.BeforeTest;	
import org.testng.annotations.AfterTest;
import org.openqa.selenium.remote.DesiredCapabilities;

import org.openqa.selenium.remote.RemoteWebDriver;

public class NewTest1 {
	
    private WebDriver driver;		
		@Test				
		public void testEasy() {	
			driver.get("http://demo.guru99.com/test/guru99home/");  
			String title = driver.getTitle();				 
			Assert.assertTrue(title.contains("Demo Guru99 Page")); 		
		}	
		@BeforeTest
		public void beforeTest() throws MalformedURLException {	
			System.setProperty("webdriver.gecko.driver", "C:\\geckodriver.exe");
		    String nodeURL = "http://192.168.100.10:4444/wd/hub";
		    DesiredCapabilities capabilities = DesiredCapabilities.firefox();
		    capabilities.setBrowserName("firefox");
		    capabilities.setPlatform(Platform.WINDOWS);
		    
		    driver = new RemoteWebDriver(new URL(nodeURL), capabilities);
		}		
		@AfterTest
		public void afterTest() {
			driver.quit();			
		}	

}

